﻿using Common.DTOs.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IProjectService : IDisposable
    {
        Task<IEnumerable<ProjectDTO>> GetAllAsync();

        Task<ProjectDTO> GetByIdAsync(int id);

        Task<ProjectDTO> CreateAsync(ProjectCreateDTO projectDTO);

        Task<ProjectDTO> UpdateAsync(ProjectUpdateDTO projectDTO);

        Task DeleteByIdAsync(int id);

        Task<Dictionary<ProjectDTO, int>> GetProjectTasksCountByUser(int Id);

        Task<IEnumerable<ProjectDetailedInfoDTO>> GetProjectsInfo();
    }
}
