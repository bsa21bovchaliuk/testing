﻿using Common.DTOs.Task;
using Common.DTOs.Team;
using Common.DTOs.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Common.DTOs.Project
{
    public class ProjectDTO
    {
        public int Id { get; set; }

        public int AuthorId { get; set; }
        public UserDTO Author { get; set; }

        public int TeamId { get; set; }
        public TeamDTO Team { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime Deadline { get; set; }

        public List<TaskDTO> Tasks { get; set; }

        public override string ToString()
        {
            return $"{"id:",-Constants.Indent}{Id}\n{"name:",-Constants.Indent}{Name}\n"
                + $"{"description:",-Constants.Indent}{Regex.Replace(Description, @"\n", $"\n{"",-Constants.Indent}")}\n"
                + $"{"createdAt:",-Constants.Indent}{CreatedAt}\n{"deadline:",-Constants.Indent}{Deadline}\n"
                + $"{"authorId:",-Constants.Indent}{AuthorId}\n"
                + $"author:\n{Author?.ToString($"{"",-Constants.Indent}")}\n"
                + $"{"teamId:",-Constants.Indent}{TeamId}\n"
                + $"team:\n{Team?.ToString($"{"",-Constants.Indent}")}\n"
                + $"tasks:\n{Tasks.ToList().Aggregate(new StringBuilder(), (current, next) => current.Append($"{next.ToString($"{"",-Constants.Indent}")}\n")).ToString()}";
        }

        public string ToString(string indent)
        {
            return Regex.Replace(ToString(), @"(?m)^", indent);
        }
    }
}
