﻿
using Common.DTOs.User;
using Common.Enums;
using System;
using System.Text.RegularExpressions;

namespace Common.DTOs.Task
{
    public class TaskDTO
    {
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public int PerformerId { get; set; }
        public UserDTO Performer { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime FinishedAt { get; set; }

        public TaskState State { get; set; }

        public override string ToString()
        {
            return $"{"id:",-Constants.Indent}{Id}\n{"name:",-Constants.Indent}{Name}\n"
                + $"{"description:",-Constants.Indent}{Regex.Replace(Description, @"\n", $"\n{"",-Constants.Indent}")}\n"
                + $"{"createdAt:",-Constants.Indent}{CreatedAt}\n{"finishedAt:",-Constants.Indent}{FinishedAt}\n"
                + $"{"state:",-Constants.Indent}{State}\n"
                + $"{"projectId:",-Constants.Indent}{ProjectId}\n{"performerId:",-Constants.Indent}{PerformerId}\n"
                + $"performer:\n{Performer?.ToString($"{"",-Constants.Indent}")}\n";
        }

        public string ToString(string indent)
        {
            return Regex.Replace(ToString(), @"(?m)^", indent);
        }
    }
}
