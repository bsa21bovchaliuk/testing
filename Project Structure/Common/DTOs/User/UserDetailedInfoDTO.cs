﻿using Common.DTOs.Project;
using Common.DTOs.Task;

namespace Common.DTOs.User
{
    public class UserDetailedInfoDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public int UnfinishedTasksCount { get; set; }
        public TaskDTO LongestTask { get; set; }

        public override string ToString()
        {
            return $"{User}\nLast Project:\n{LastProject?.ToString($"{"",-Constants.Indent}")}\n"
                   + $"Last Project Tasks Count:\t{LastProjectTasksCount}\n"
                   + $"Unfinished Tasks Count:\t\t{UnfinishedTasksCount}\n"
                   + $"Longest task by duration:\n{LongestTask?.ToString($"{"",-Constants.Indent}")}\n";
        }
    }
}
